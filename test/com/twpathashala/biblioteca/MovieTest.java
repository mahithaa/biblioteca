package com.twpathashala.biblioteca;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MovieTest {
    @Test
    public void shouldReturnDetailsWhenToStringIsCalled() {
        Movie movie = new Movie("Googly", "pavan", "2013", "8");
        String output = "Googly pavan 2013 8";
        assertEquals(output, movie.toString());
    }

    @Test
    public void shouldReturnIfMovieNameIsSame() {
        Movie movie = new Movie("Googly", "pavan", "2013", "8");
        assertTrue(movie.compareName("Googly"));
    }

    @Test
    public void shouldReturnIfBookNameIsNotSame() {
        Movie movie = new Movie("Googly", "pavan", "2013", "8");
        assertFalse(movie.compareName("first"));
    }
}
