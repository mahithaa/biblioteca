package com.twpathashala.biblioteca;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

public class UserAuthenicatorTest {
    @Test
    public void validateUserShouldReturnUserWhenUserinUserList() {
        ArrayList<User> users = new ArrayList<>();
        User user = mock(User.class);
        users.add(user);
        UserAuthenicator validate = new UserAuthenicator((users));
        assertEquals(validate.authenticate(user), user);
    }

    @Test
    public void validateUserShouldReturnNullWhenUserNotinUserList() {
        ArrayList<User> users = new ArrayList<>();
        User user = mock(User.class);
        UserAuthenicator validate = new UserAuthenicator((users));
        assertNull(validate.authenticate(user));
    }

}

