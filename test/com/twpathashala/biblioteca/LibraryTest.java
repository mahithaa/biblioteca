package com.twpathashala.biblioteca;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class LibraryTest {
    static final String ALGORITHMS = "Algorithms";
    static final String BANGALOREDAYS = "Bangaloredays";
    private Input input;
    private Output output;
    private Library library;

    @Before
    public void setUp() {
        input = mock(Input.class);
        output = mock(Output.class);
    }

    @Test
    public void checkOutBookShouldReturnBookWhenBookIsAvailable() {
        ArrayList<Book> books = new ArrayList<>();
        Book book = mock(Book.class);
        books.add(book);
        ArrayList<Movie> movies = new ArrayList<>();
        library = new Library(books, movies, output);
        when(books.get(0).compareName(ALGORITHMS)).thenReturn(true);
        assertTrue(book.equals(library.checkoutBook(ALGORITHMS)));
    }

    @Test
    public void checkOutBookShouldReturnEmtpyBookWhenBookIsNotInLibrary() {
        ArrayList<Book> books = new ArrayList<>();
        ArrayList<Movie> movies = new ArrayList<>();
        library = new Library(books, movies, output);
        assertEquals(new Book("", "", ""), library.checkoutBook(ALGORITHMS));
    }

    @Test
    public void checkOutMovieShouldReturnMovieWhenMovieIsAvailable() {
        ArrayList<Book> books = new ArrayList<>();
        ArrayList<Movie> movies = new ArrayList<>();
        Movie movie = mock(Movie.class);
        movies.add(movie);
        library = new Library(books, movies, output);
        when(movies.get(0).compareName(BANGALOREDAYS)).thenReturn(true);
        assertTrue(movie.equals(library.checkoutMovie(BANGALOREDAYS)));
    }

    @Test
    public void checkOutMovieShouldReturnEmptyMovieWhenMovieIsNotInLibrary() {
        ArrayList<Book> books = new ArrayList<>();
        ArrayList<Movie> movies = new ArrayList<>();
        library = new Library(books, movies, output);
        assertEquals(new Movie("", "", "", ""), library.checkoutMovie(ALGORITHMS));
    }

    @Test
    public void shouldIncreaseSizeBy1WhenBookIsAdded() {
        ArrayList<Book> books = new ArrayList<>();
        ArrayList<Movie> movies = new ArrayList<>();
        library = new Library(books, movies, output);
        Book book = mock(Book.class);
        library.returnBook(book);
        assertEquals(1, books.size());
    }
}
