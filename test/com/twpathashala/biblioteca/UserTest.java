package com.twpathashala.biblioteca;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserTest {
    @Test
    public void shouldReturnFalseIfUserIdAndPwdAreNotSame() {
        User user1 = new User("abc-user", "abc", "mail", "ph");
        User user2 = new User("bca-user", "bca", "mail", "ph");
        assertFalse(user1.equals(user2));
    }

    @Test
    public void shouldReturnTrueIfUserIdAndPwdAreSame() {
        User user1 = new User("abc-user", "abc", "Mail", "phNo");
        User user2 = new User("abc-user", "abc", "Mail", "pNo");
        assertTrue(user1.equals(user2));
    }

    @Test
    public void shouldReturnIdWhenToStringMethodIsCalled() {
        User user = new User("abc-user", "abc", "mailId", "phNo");
        assertEquals("abc-user", user.getId());
    }
}
