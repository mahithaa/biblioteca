package com.twpathashala.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieParserTest {
    @Test
    public void shouldReturnMovieNameIsGoogly() throws IOException {
        Output output = mock(Output.class);
        BufferedReader reader = mock(BufferedReader.class);
        MovieParser parser = new MovieParser(output, reader);
        when(reader.readLine()).thenReturn("Googly,pavan,2013,8").thenReturn(null);
        Movie movie = mock(Movie.class);
        when(movie.compareName("Googly")).thenReturn(true);
        assertTrue(parser.readFile().get(0).compareName("Googly"));

    }
}
