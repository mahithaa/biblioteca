package com.twpathashala.biblioteca;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LibrarianTest {
    static final String ALGORITHMS = "Algorithms";
    static final String GOOGLY = "Googly";
    static final String USER_ID = "123";
    private User user;
    private Library library;
    private Librarian librarian;

    @Before
    public void setUp() {
        user = mock(User.class);
        library = mock(Library.class);
        librarian = new Librarian(library);
    }

    @Test
    public void chekOutshouldReturnEnjoyBookWhenBookisinLibrary() {
        Book book = mock(Book.class);
        when(library.checkoutBook(ALGORITHMS)).thenReturn(book);
        assertEquals(DisplayMessage.BOOKSUCCESSFULCHECKOUT.toString(), librarian.checkoutBook(user, ALGORITHMS));
    }

    @Test
    public void chekOutBookshouldReturnNotAvailableWhenBookisAlreadyCheckedout() {
        Book book = mock(Book.class);
        when(library.checkoutBook(ALGORITHMS)).thenReturn(book);
        librarian.checkoutBook(user, ALGORITHMS);
        when(book.compareName(ALGORITHMS)).thenReturn(true);
        assertEquals(DisplayMessage.BOOKNOTAVAILABLE.toString(), librarian.checkoutBook(user, ALGORITHMS));
    }

    @Test
    public void chekOutBookshouldReturnDoesNotExistWhenBookisNotinLibrary() {
        when(library.checkoutBook(ALGORITHMS)).thenReturn(new Book("", "", ""));
        assertEquals(DisplayMessage.BOOKDOESNOTEXIST.toString(), librarian.checkoutBook(user, ALGORITHMS));
    }

    @Test
    public void chekOutshouldReturnEnjoyMovieWhenMovieisinLibrary() {
        Movie movie = mock(Movie.class);
        when(library.checkoutMovie(GOOGLY)).thenReturn(movie);
        assertEquals(DisplayMessage.MOVIESUCCESSFULCHECKOUT.toString(), librarian.checkoutMovie(user, GOOGLY));
    }

    @Test
    public void chekOutMovieshouldReturnNotAvailableWhenMovieisAlreadyCheckedout() {
        Movie movie = mock(Movie.class);
        when(library.checkoutMovie(GOOGLY)).thenReturn(movie);
        librarian.checkoutMovie(user, GOOGLY);
        when(movie.compareName(GOOGLY)).thenReturn(true);
        assertEquals(DisplayMessage.MOVIENOTAVAILABLE.toString(), librarian.checkoutMovie(user, GOOGLY));
    }

    @Test
    public void chekOutMoviehouldReturnDoesNotExistWhenMovieisNotinLibrary() {
        when(library.checkoutMovie(GOOGLY)).thenReturn(new Movie("", "", "", ""));
        assertEquals(DisplayMessage.MOVIEDOESNOTEXIST.toString(), librarian.checkoutMovie(user, GOOGLY));
    }

    @Test
    public void shouldReturnThankYouForReturningCorrectBook() {
        Book book = mock(Book.class);
        when(library.checkoutBook(ALGORITHMS)).thenReturn(book);
        when(user.getId()).thenReturn(USER_ID);
        when(book.compareName(ALGORITHMS)).thenReturn(true);
        librarian.checkoutBook(user, ALGORITHMS);
        assertEquals(DisplayMessage.BOOKSUCCESSFULCHECKIN.toString(), librarian.returnBook(user, ALGORITHMS));
    }

    @Test
    public void shouldReturnNotAValidWhenReturningBookIsNotCheckedOut() {
        assertEquals(DisplayMessage.BOOKUNSUCCESSFULCHECKIN.toString(), librarian.returnBook(user, "Java"));
    }
}
