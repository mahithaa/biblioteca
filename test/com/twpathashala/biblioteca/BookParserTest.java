package com.twpathashala.biblioteca;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BookParserTest {
    @Test
    public void shouldReturnBookNameIsHeadFirstJava() throws IOException {
        Output output = mock(Output.class);
        BufferedReader reader = mock(BufferedReader.class);
        BookParser parser = new BookParser(output, reader);
        when(reader.readLine()).thenReturn("Head first java,Kathy Sierra,1995").thenReturn(null);
        Book book = mock(Book.class);
        when(book.compareName("Head first java")).thenReturn(true);
        assertTrue(parser.readFile().get(0).compareName("Head first java"));
    }
}
