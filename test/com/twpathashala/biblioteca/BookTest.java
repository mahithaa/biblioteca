package com.twpathashala.biblioteca;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BookTest {
    @Test
    public void shouldReturnDetailsWhenToStringIsCalled() {
        Book book = new Book("Java", "yyy", "1990");
        String output = "Java yyy 1990";
        assertEquals(output, book.toString());
    }

    @Test
    public void shouldReturnIfBookNameIsSame() {
        Book book = new Book("Java", "yyy", "1990");
        assertTrue(book.compareName("Java"));
    }

    @Test
    public void shouldReturnIfBookNameIsNotSame() {
        Book book = new Book("Java", "yyy", "1990");
        assertFalse(book.compareName("first"));
    }
}
