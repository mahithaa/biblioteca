package com.twpathashala.biblioteca;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class ControllerTest {
    private Library library;
    private Output output;
    private Input input;
    private User user;
    private Librarian librarian;
    private Controller controller;

    @Before
    public void setUp() {
        library = mock(Library.class);
        output = new Output();
        input = mock(Input.class);
        user = mock(User.class);
        librarian = mock(Librarian.class);
        controller = new Controller(library, output, input, librarian, user);
    }

    @Test
    public void shouldDisplayBookDetailsIfChoiceIs1() {
        controller.options(1);
        verify(library).bookDetails();
    }

    @Test
    public void shouldExitIfChoiceIs0() {
        controller.options(0);
        verify(library, never()).bookDetails();
    }

    @Test
    public void shouldCallCheckoutBookMethodIfOptionIs2() {
        when(input.getName()).thenReturn("Algorithms");
        controller.options(2);
        verify(librarian).checkoutBook(user, "Algorithms");
    }

    @Test
    public void shouldCallReturnBookMethodIfOptionIs3() {
        when(input.getName()).thenReturn("Algorithms");
        controller.options(3);
        verify(librarian).returnBook(user, "Algorithms");
    }

    @Test
    public void shouldDisplayMovieDetailsIfChoiceIs2() {
        controller.options(4);
        verify(library).movieDetails();
    }

    @Test
    public void shouldCallCheckoutMovieMethodIfOptionIs5() {
        when(input.getName()).thenReturn("julai");
        controller.options(5);
        verify(librarian).checkoutMovie(user, "julai");
    }

    @Test
    public void shouldDisplaySelectValidIfChoiceIsMoreThan6() {
        controller.options(10);
        verify(library, never()).bookDetails();
    }
}
