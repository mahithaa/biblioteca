package com.twpathashala.biblioteca;

// Understand operation to perform
class Controller {
    private Library library;
    private Output output;
    private Input input;
    private Librarian librarian;
    private User user;

    Controller(Library library, Output output, Input input, Librarian librarian, User user) {
        this.library = library;
        this.output = output;
        this.input = input;
        this.librarian = librarian;
        this.user = user;
    }

    void options(int choice) {
        switch (choice) {
            case 0:
                System.exit(0);
            case 1: {
                library.bookDetails();
                break;
            }
            case 2: {
                String bookName = input.getName();
                output.print(librarian.checkoutBook(user, bookName));
                break;
            }
            case 3: {
                String bookName = input.getName();
                output.print(librarian.returnBook(user, bookName));
                break;
            }
            case 4: {
                library.movieDetails();
                break;
            }
            case 5: {
                String movieName = input.getName();
                output.print(librarian.checkoutMovie(user, movieName));
                break;
            }
            case 6: {
                output.print(user.toString());
                break;
            }
            default:
                output.print("Select valid option");
        }
    }
}
