package com.twpathashala.biblioteca;

import java.util.Scanner;

//Understand data form console
class Input {
    Output output;
    private Scanner scanner;

    Input(Output output, Scanner scanner) {
        this.output = output;
        this.scanner = scanner;
    }

    int getOption() {
        return scanner.nextInt();
    }

    String getName() {
        output.print("Enter name");
        return scanner.next();
    }

    String[] getUserDetails() {
        String user[] = new String[2];
        output.print("Enter user details");
        output.print("Id::");
        user[0] = scanner.next();
        output.print("Password");
        user[1] = scanner.next();
        return user;
    }
}
