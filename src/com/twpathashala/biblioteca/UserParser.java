package com.twpathashala.biblioteca;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

//Understands the input from file
class UserParser implements Parser {
    private Output output;
    private BufferedReader reader;

    UserParser(Output output, BufferedReader reader) {
        this.output = output;
        this.reader = reader;
    }

    @Override
    public ArrayList<User> readFile() {
        ArrayList<User> users = new ArrayList<>();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                StringTokenizer userDetails = new StringTokenizer(line, ",");
                while (userDetails.hasMoreTokens()) {
                    users.add(new User(userDetails.nextToken(), userDetails.nextToken(), userDetails.nextToken(), userDetails.nextToken()));
                }

            }
        } catch (Exception e) {
            output.print("File not found");
        }

        return users;
    }
}
