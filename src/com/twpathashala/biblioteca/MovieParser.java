package com.twpathashala.biblioteca;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

//Understands the input from file
class MovieParser implements Parser {
    private Output output;
    private BufferedReader reader;

    MovieParser(Output output, BufferedReader reader) {
        this.output = output;
        this.reader = reader;
    }

    @Override
    public ArrayList<Movie> readFile() {
        ArrayList<Movie> movies = new ArrayList<>();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                StringTokenizer movieDetails = new StringTokenizer(line, ",");
                while (movieDetails.hasMoreTokens()) {
                    movies.add(new Movie(movieDetails.nextToken(), movieDetails.nextToken(), movieDetails.nextToken(), movieDetails.nextToken()));
                }
            }
        } catch (Exception e) {
            output.print("File not found");
        }

        return movies;
    }
}
