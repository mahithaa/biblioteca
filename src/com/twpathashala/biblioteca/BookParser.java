package com.twpathashala.biblioteca;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

//Reads the input from file
class BookParser implements Parser {
    private Output output;
    private BufferedReader reader;

    BookParser(Output output, BufferedReader reader) {
        this.output = output;
        this.reader = reader;
    }

    public ArrayList<Book> readFile() {
        ArrayList<Book> books = new ArrayList<>();
        String line;
        try {

            while ((line = reader.readLine()) != null) {
                StringTokenizer bookDetails = new StringTokenizer(line, ",");
                while (bookDetails.hasMoreTokens()) {
                    books.add(new Book(bookDetails.nextToken(), bookDetails.nextToken(), bookDetails.nextToken()));
                }
            }
        } catch (Exception e) {
            output.print("File not found");
        }
        return books;
    }
}
