package com.twpathashala.biblioteca;

//Holds the properties of product

class Movie {
    private String name;
    private String director;
    private String year;
    private String rating;

    Movie(String name, String director, String year, String rating) {
        this.name = name;
        this.director = director;
        this.year = year;
        this.rating = rating;
    }

    public boolean compareName(String movieName) {
        return (name.equals(movieName));
    }

    @Override
    public String toString() {
        return name + " " + director + " " + year + " " + rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        return name.equals(movie.name) && director.equals(movie.director) && year.equals(movie.year) && rating.equals(movie.rating);
    }
}