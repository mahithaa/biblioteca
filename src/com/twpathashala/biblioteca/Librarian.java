package com.twpathashala.biblioteca;

import java.util.HashMap;
import java.util.Map;

//Understands the operations of library

class Librarian {
    private Library library;
    private Book EMPTY_BOOK = new Book("", "", "");
    private Movie EMPTY_MOVIE = new Movie("", "", "", "");
    private HashMap<Book, String> bookRegister = new HashMap<>();
    private HashMap<Movie, String> movieRegister = new HashMap<>();

    Librarian(Library library) {
        this.library = library;
    }

    String checkoutBook(User user, String bookName) {
        if (isBookCheckedOut(bookName)) {
            return DisplayMessage.BOOKNOTAVAILABLE.toString();
        }
        Book book = library.checkoutBook(bookName);
        if (EMPTY_BOOK.equals(book)) {
            return DisplayMessage.BOOKDOESNOTEXIST.toString();
        }
        bookRegister.put(book, user.getId());
        return DisplayMessage.BOOKSUCCESSFULCHECKOUT.toString();
    }

    String checkoutMovie(User user, String movieName) {
        if (isMovieCheckedOut(movieName)) {
            return DisplayMessage.MOVIENOTAVAILABLE.toString();
        }
        Movie movie = library.checkoutMovie(movieName);
        if (EMPTY_MOVIE.equals(movie)) {
            return DisplayMessage.MOVIEDOESNOTEXIST.toString();
        }
        movieRegister.put(movie, user.getId());
        return DisplayMessage.MOVIESUCCESSFULCHECKOUT.toString();
    }

    String returnBook(User user, String bookName) {
        for (Map.Entry<Book, String> list : bookRegister.entrySet()) {
            if (list.getKey().compareName(bookName) && list.getValue().equals(user.getId())) {
                library.returnBook(list.getKey());
                bookRegister.remove(list.getKey());
                return DisplayMessage.BOOKSUCCESSFULCHECKIN.toString();
            }
        }
        return DisplayMessage.BOOKUNSUCCESSFULCHECKIN.toString();
    }

    private boolean isBookCheckedOut(String bookName) {
        for (Map.Entry<Book, String> list : bookRegister.entrySet()) {
            if (list.getKey().compareName(bookName))
                return true;
        }
        return false;
    }

    private boolean isMovieCheckedOut(String movieName) {
        for (Map.Entry<Movie, String> list : movieRegister.entrySet()) {
            if (list.getKey().compareName(movieName))
                return true;
        }
        return false;
    }
}
