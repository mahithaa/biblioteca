package com.twpathashala.biblioteca;

//Understands the operations on library
class Menu {
    private Output output;

    Menu(Output output) {
        this.output = output;
    }

    void list() {
        output.print("0.Exit");
        output.print("1.List of Books");
        output.print("2.Checkout Book");
        output.print("3.Return Book");
        output.print("4.List of Movies");
        output.print("5.Checkout movie");
        output.print("6.Customer Details");
    }
}
