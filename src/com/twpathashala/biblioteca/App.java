package com.twpathashala.biblioteca;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

// Create the required object
public class App {
    private Controller controller;
    private UserAuthenicator userAuthenicator;
    private Output output;
    private Input input;
    private User user;
    private Menu menu;
    private BufferedReader bookReader, movieReader, userReader;

    public void start() {
        output = new Output();
        Scanner scanner = new Scanner(System.in);
        menu = new Menu(output);
        fileReader();
        Parser parser = new BookParser(output, bookReader);
        input = new Input(output, scanner);
        ArrayList<Book> books = (ArrayList<Book>) parser.readFile();
        parser = new MovieParser(output, movieReader);
        ArrayList<Movie> movies = (ArrayList<Movie>) parser.readFile();
        parser = new UserParser(output, userReader);
        ArrayList<User> users = (ArrayList<User>) parser.readFile();
        userAuthenicator = new UserAuthenicator(users);
        Library library = new Library(books, movies, output);
        output.print("Welcome to Biblioteca");
        logIn();
        Librarian librarian = new Librarian(library);
        controller = new Controller(library, output, input, librarian, user);
        option(input);
    }

    private void logIn() {
        String[] userId = input.getUserDetails();
        user = userAuthenicator.authenticate(new User(userId[0], userId[1], "", ""));
        if (new User("","","","").equals(user)) {
            invalidUser();
            logIn();
        }
    }

    private void option(Input input) {
        while (true) {
            menu.list();
            output.print("Enter option");
            int option = input.getOption();
            controller.options(option);
        }
    }

    private void invalidUser() {
        output.print("Entered details are not valid\n 0.Exit 1.Re-enter ");
        int option = input.getOption();
        if (option != 1) {
            controller.options(0);
        }
    }

    private void fileReader() {
        try {
            bookReader = new BufferedReader(new FileReader("resource/booklist"));
            movieReader = new BufferedReader(new FileReader("resource/movielist"));
            userReader = new BufferedReader(new FileReader("resource/userslist"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
