package com.twpathashala.biblioteca;

import java.util.ArrayList;

class UserAuthenicator {
    private ArrayList<User> users;

    UserAuthenicator(ArrayList<User> users) {
        this.users = users;
    }

    User authenticate(User otherUser) {
        for (User user : users) {
            if (user.equals(otherUser)) {
                return user;
            }
        }
        return null;
    }
}
