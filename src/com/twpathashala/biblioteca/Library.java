package com.twpathashala.biblioteca;

import java.util.*;

//Holds the list of books and movies

class Library {

    private ArrayList<Book> books;
    private ArrayList<Movie> movies;
    private Output output;

    Library(ArrayList<Book> books, ArrayList<Movie> movies, Output output) {
        this.books = books;
        this.movies = movies;
        this.output = output;
    }

    void bookDetails() {
        for (Book book : books) {
            output.print(book.toString());
        }
    }

    Book checkoutBook(String bookName) {
        for (Book book : books) {
            if (book.compareName(bookName)) {
                books.remove(book);
                return book;
            }
        }
        return new Book("", "", "");
    }

    void returnBook(Book book) {
        books.add(book);
    }

    void movieDetails() {
        for (Movie movie : movies) {
            output.print(movie.toString());
        }
    }

    Movie checkoutMovie(String movieName) {
        for (Movie movie : movies) {
            if (movie.compareName(movieName)) {
                movies.remove(movie);
                return movie;
            }
        }
        return new Movie("","","","");
    }
}
