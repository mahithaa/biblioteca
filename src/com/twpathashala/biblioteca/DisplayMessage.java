package com.twpathashala.biblioteca;

enum DisplayMessage {

    BOOKSUCCESSFULCHECKOUT("Enjoy book"),
    BOOKNOTAVAILABLE("Book is Not Available"),
    BOOKDOESNOTEXIST("Book Does not exist"),
    MOVIESUCCESSFULCHECKOUT("Enjoy Movie"),
    MOVIENOTAVAILABLE("Movie is Not Available"),
    MOVIEDOESNOTEXIST("Movie Does not exist"),
    BOOKSUCCESSFULCHECKIN("Thank you for returning book"),
    BOOKUNSUCCESSFULCHECKIN("This is not a valid book to return");

    String message;

    DisplayMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}

