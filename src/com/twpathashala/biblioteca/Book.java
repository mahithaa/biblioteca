package com.twpathashala.biblioteca;

// Holds details of product
class Book {
    private String name;
    private String author;
    private String year;

    Book(String name, String author, String year) {
        this.name = name;
        this.author = author;
        this.year = year;
    }

    boolean compareName(String bookName) {
        return (name.equals(bookName));
    }

    @Override
    public String toString() {
        return name + " " + author + " " + year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (name != null ? !name.equals(book.name) : book.name != null) return false;
        if (author != null ? !author.equals(book.author) : book.author != null) return false;
        return year != null ? year.equals(book.year) : book.year == null;
    }
}
