package com.twpathashala.biblioteca;

//Understands the properies of the library customer
class User {
    private String id;
    private String pwd;
    private String mailId;
    private String phNo;

    User(String id, String pwd, String mailId, String phNo) {
        this.id = id;
        this.pwd = pwd;
        this.mailId = mailId;
        this.phNo = phNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        return pwd != null ? pwd.equals(user.pwd) : user.pwd == null;
    }

    @Override
    public String toString() {
        return id + " " + mailId + " " + phNo;
    }

    String getId() {
        return id;
    }
}
